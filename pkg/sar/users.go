package sar

import (
	"encoding/json"
	"fmt"
)

type RegisteredUserResponse struct {
	ID       string `json:"id"`
	Username string `json:"username"`
	Approved bool   `json:"approved"`
}

func (c *APIClient) RegisterUser(username, password string) (*RegisteredUserResponse, error) {
	if username == "" {
		return nil, fmt.Errorf("Username cannot be empty")
	}

	if password == "" {
		return nil, fmt.Errorf("Password cannot be empty")
	}

	endpoint := "api/users/register"
	requestBody := map[string]string{
		"username": username,
		"password": password,
	}

	response, err := PerformHTTPPost(c.URL, endpoint, c.Token, requestBody)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result RegisteredUserResponse
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (c *APIClient) ApproveUser(username string) (*RegisteredUserResponse, error) {
	if username == "" {
		return nil, fmt.Errorf("Username cannot be empty")
	}

	endpoint := fmt.Sprintf("api/users/%s/approve", username)
	response, err := PerformHTTPPost(c.URL, endpoint, c.Token, nil)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result RegisteredUserResponse
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (c *APIClient) AssignRoleToUser(username, rolename string) (*GenericResponse, error) {
	if username == "" {
		return nil, fmt.Errorf("Username cannot be empty")
	}

	if rolename == "" {
		return nil, fmt.Errorf("Rolename cannot be empty")
	}

	endpoint := fmt.Sprintf("api/users/%s/addrole", username)
	requestBody := map[string]string{
		"RoleName": rolename,
	}

	response, err := PerformHTTPPost(c.URL, endpoint, c.Token, requestBody)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result GenericResponse
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (c *APIClient) RemoveRoleFromUser(username, rolename string) (*GenericResponse, error) {
	if username == "" {
		return nil, fmt.Errorf("Username cannot be empty")
	}

	if rolename == "" {
		return nil, fmt.Errorf("Rolename cannot be empty")
	}

	endpoint := fmt.Sprintf("api/users/%s/removerole", username)
	requestBody := map[string]string{
		"roleName": rolename,
	}

	response, err := PerformHTTPPost(c.URL, endpoint, c.Token, requestBody)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result GenericResponse
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (c *APIClient) RemoveUser(username string) (*GenericResponse, error) {
	if username == "" {
		return nil, fmt.Errorf("Username cannot be empty")
	}

	endpoint := fmt.Sprintf("api/users/%s", username)

	response, err := PerformHTTPDelete(c.URL, endpoint, c.Token)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result GenericResponse
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (c *APIClient) GetUserByName(username string) (*RegisteredUserResponse, error) {
	if username == "" {
		return nil, fmt.Errorf("Username cannot be empty")
	}

	endpoint := fmt.Sprintf("api/users/%s", username)

	response, err := PerformHTTPGet(c.URL, endpoint, c.Token)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result RegisteredUserResponse
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (c *APIClient) ListUsers() ([]RegisteredUserResponse, error) {
	endpoint := "api/users"

	response, err := PerformHTTPGet(c.URL, endpoint, c.Token)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var results []RegisteredUserResponse
	err = json.NewDecoder(response.Body).Decode(&results)
	if err != nil {
		return nil, err
	}

	return results, nil
}
