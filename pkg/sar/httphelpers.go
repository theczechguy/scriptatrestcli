package sar

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

// PerformHTTPPost sends an HTTP POST request and returns the response.
func PerformHTTPPost(url, endpoint, token string, requestBody interface{}) (*http.Response, error) {
	fullEndpoint := fmt.Sprintf("%s/%s", url, endpoint)

	requestBodyBytes, err := json.Marshal(requestBody)
	if err != nil {
		return nil, err
	}

	request, err := http.NewRequest("POST", fullEndpoint, bytes.NewBuffer(requestBodyBytes))
	if err != nil {
		return nil, err
	}

	request.Header.Set("Content-Type", "application/json")
	if token != "" {
		request.Header.Set("Authorization", "Bearer "+token)
	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return nil, err
	}

	return checkResponse(response)
}

func PerformHTTPDelete(url, endpoint, token string) (*http.Response, error) {
	fullEndpoint := fmt.Sprintf("%s/%s", url, endpoint)

	request, err := http.NewRequest(http.MethodDelete, fullEndpoint, nil)
	if err != nil {
		return nil, err
	}

	request.Header.Set("Content-Type", "application/json")
	if token != "" {
		request.Header.Set("Authorization", "Bearer "+token)
	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return nil, err
	}

	return checkResponse(response)
}

// PerformHTTPGet sends an HTTP GET request and returns the response.
func PerformHTTPGet(url, endpoint, token string) (*http.Response, error) {
	fullEndpoint := fmt.Sprintf("%s/%s", url, endpoint)

	request, err := http.NewRequest("GET", fullEndpoint, nil)
	if err != nil {
		return nil, err
	}

	if token != "" {
		request.Header.Set("Authorization", "Bearer "+token)
	}
	request.Header.Set("Accept", "application/json")

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return nil, err
	}

	return checkResponse(response)
}

func PerformHTTPPut(url, endpoint, token string, requestBody interface{}) (*http.Response, error) {
	fullEndpoint := fmt.Sprintf("%s/%s", url, endpoint)

	requestBodyBytes, err := json.Marshal(requestBody)
	if err != nil {
		return nil, err
	}

	request, err := http.NewRequest("PUT", fullEndpoint, bytes.NewBuffer(requestBodyBytes))
	if err != nil {
		return nil, err
	}

	request.Header.Set("Content-Type", "application/json")
	if token != "" {
		request.Header.Set("Authorization", "Bearer "+token)
	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return nil, err
	}

	return checkResponse(response)
}

// checkResponse checks the HTTP response status code and returns an error if necessary.
func checkResponse(response *http.Response) (*http.Response, error) {
	if response.StatusCode < 200 || response.StatusCode >= 300 {
		body, readErr := io.ReadAll(response.Body)
		if readErr != nil {
			return response, fmt.Errorf("Failed to read response body: %v", readErr)
		}

		errMessage := fmt.Sprintf("Request failed with status code %d | %s", response.StatusCode, getErrorMessage(response.StatusCode))
		if len(body) > 0 {
			errMessage = fmt.Sprintf("%s | Response Body: %s", errMessage, string(body))
		}
		return response, fmt.Errorf(errMessage)
	}
	return response, nil
}

// getErrorMessage returns a human-readable error message for a given HTTP status code.
func getErrorMessage(statusCode int) string {
	errorMessages := map[int]string{
		http.StatusBadRequest:          "Bad Request",
		http.StatusUnauthorized:        "Unauthorized",
		http.StatusForbidden:           "Forbidden",
		http.StatusNotFound:            "Not Found",
		http.StatusMethodNotAllowed:    "Method Not Allowed",
		http.StatusRequestTimeout:      "Request Timeout",
		http.StatusInternalServerError: "Internal Server Error",
		http.StatusServiceUnavailable:  "Service Unavailable",
		// Add more status codes as needed
	}

	message, found := errorMessages[statusCode]
	if !found {
		return "Unknown Error"
	}

	return message
}
