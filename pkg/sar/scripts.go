package sar

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"
)

type Script struct {
	ID              int             `json:"id"`
	Name            string          `json:"name"`
	EncodedContent  string          `json:"encodedContent"`
	ScriptType      ScriptType      `json:"scriptType"`
	Timeout         int             `json:"timeout"`
	LastModifiedUtc time.Time       `json:"lastModifiedUtc"`
	ScriptHistory   []ScriptHistory `json:"scriptHistory"`
}
type SimpleScript struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type ScriptHistory struct {
	ID             int        `json:"id"`
	ScriptEntityID int        `json:"scriptEntityId"`
	Name           string     `json:"name"`
	EncodedContent string     `json:"encodedContent"`
	ScriptType     ScriptType `json:"scriptType"`
	Timeout        int        `json:"timeout"`
	ModifiedUtc    time.Time  `json:"modifiedUtc"`
}

func (c *APIClient) RegisterScript(name, typeId, encodedContent, timeoutSeconds string) (*Script, error) {

	endpoint := "api/script/register"
	requestBody := map[string]string{
		"name":           name,
		"type":           typeId,
		"encodedContent": encodedContent,
		"timeout":        timeoutSeconds,
	}

	response, err := PerformHTTPPost(c.URL, endpoint, c.Token, requestBody)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result Script
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (c *APIClient) DeleteScript(id string) (bool, error) {

	endpoint := fmt.Sprintf("api/script/%s", id)
	_, err := PerformHTTPDelete(c.URL, endpoint, c.Token)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (c *APIClient) ListScripts() ([]SimpleScript, error) {

	endpoint := "api/script"
	response, err := PerformHTTPGet(c.URL, endpoint, c.Token)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result []SimpleScript
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (c *APIClient) GetScript(id string) (*Script, error) {

	endpoint := fmt.Sprintf("api/script/%s", id)
	response, err := PerformHTTPGet(c.URL, endpoint, c.Token)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result Script
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (c *APIClient) UpdateScript(id int, name, typeId, encodedContent, timeoutSeconds string) (*Script, error) {

	endpoint := fmt.Sprintf("api/script/%s", strconv.Itoa(id))
	requestBody := make(map[string]string)

	if name != "" {
		requestBody["name"] = name
	}
	if typeId != "" {
		requestBody["type"] = typeId
	}
	if encodedContent != "" {
		requestBody["encodedContent"] = encodedContent
	}
	if timeoutSeconds != "" {
		requestBody["timeout"] = timeoutSeconds
	}

	response, err := PerformHTTPPut(c.URL, endpoint, c.Token, requestBody)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result Script
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}
