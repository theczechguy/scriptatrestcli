package sar

type GenericResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

type APIClient struct {
	URL   string
	Token string
}
