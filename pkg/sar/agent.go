package sar

import (
	"encoding/json"
	"fmt"
	"time"
)

type AgentRegisterResponse struct {
	AgentName string `json:"agentName"`
	AgentID   string `json:"agentId"`
	Token     string `json:"token"`
}

type Agent struct {
	ID                 string `json:"id"`
	UserDefinedName    string `json:"userDefinedName"`
	ConnectionStatus   string `json:"connectionStatus"`
	LastConnected      string `json:"lastConnected"`
	ConnectionID       any    `json:"connectionId"`
	AgentCapabilityIds []any  `json:"agentCapabilityIds"`
}

type AgentToken struct {
	Token                  string    `json:"token"`
	TokenExpiration        time.Time `json:"tokenExpiration"`
	RefreshToken           string    `json:"refreshToken"`
	RefreshTokenExpiration time.Time `json:"refreshTokenExpiration"`
}
type ConnectionToken struct {
	Token string `json:"token"`
}

func (c *APIClient) RegisterAgent(name string) (*AgentRegisterResponse, error) {

	endpoint := "api/agent/register"
	requestBody := map[string]string{"agentName": name}

	response, err := PerformHTTPPost(c.URL, endpoint, c.Token, requestBody)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result AgentRegisterResponse
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (c *APIClient) ListAgents() (*[]Agent, error) {

	endpoint := "api/agent"

	response, err := PerformHTTPGet(c.URL, endpoint, c.Token)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result []Agent
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (c *APIClient) GetAgentById(agentId string) (*Agent, error) {

	endpoint := fmt.Sprintf("api/agent/%s", agentId)

	response, err := PerformHTTPGet(c.URL, endpoint, c.Token)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result Agent
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (c *APIClient) RefreshAgentToken(refreshToken string) (*AgentToken, error) {

	endpoint := "api/Agent/refreshtoken"
	requestBody := map[string]string{"token": refreshToken}

	response, err := PerformHTTPPost(c.URL, endpoint, c.Token, requestBody)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result AgentToken
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}
func (c *APIClient) ResetAgentToken(agentId string) (*ConnectionToken, error) {

	endpoint := fmt.Sprintf("api/agent/%s/resettoken", agentId)

	response, err := PerformHTTPPost(c.URL, endpoint, c.Token, nil)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result ConnectionToken
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}
