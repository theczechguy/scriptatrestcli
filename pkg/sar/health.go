package sar

import (
	"encoding/json"
)

type HealthStatus struct {
	Answer     string `json:"answer"`
	AnswerDate string `json:"answerDate"`
}

func (c *APIClient) CheckHealth() (*HealthStatus, error) {

	endpoint := "api/Health"

	response, err := PerformHTTPGet(c.URL, endpoint, c.Token)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result HealthStatus
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}
