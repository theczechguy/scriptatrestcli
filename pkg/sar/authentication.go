package sar

import (
	"encoding/json"
	"fmt"
	"time"
)

type AuthenticationResponse struct {
	Token                  string    `json:"token"`
	TokenExpiration        time.Time `json:"tokenExpiration"`
	RefreshToken           string    `json:"refreshToken"`
	RefreshTokenExpiration time.Time `json:"refreshTokenExpiration"`
}

func Login(url, username, password string) (*AuthenticationResponse, error) {
	endpoint := "api/Authentication/login"
	requestBody := map[string]string{
		"username": username,
		"password": password,
	}

	response, err := PerformHTTPPost(url, endpoint, "", requestBody)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	if response.StatusCode < 200 || response.StatusCode >= 300 {
		return nil, fmt.Errorf("Authentication failed with status code %d", response.StatusCode)
	}

	var result AuthenticationResponse
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func (c *APIClient) GenerateTokens(username, password string) (*AuthenticationResponse, error) {
	return Login(c.URL, username, password)
}

func RefreshTokens(url, refreshToken string) (*AuthenticationResponse, error) {
	endpoint := "api/Authentication/refreshtoken"
	requestBody := map[string]string{
		"refreshToken": refreshToken,
	}

	response, err := PerformHTTPPost(url, endpoint, "", requestBody)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	if response.StatusCode < 200 || response.StatusCode >= 300 {
		return nil, fmt.Errorf("Token refresh failed with status code %d", response.StatusCode)
	}

	var result AuthenticationResponse
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}

	return &result, nil
}

func RevokeRefreshToken(url, token, refreshToken string) (bool, error) {
	endpoint := "api/Authentication/revoketoken"
	requestBody := map[string]string{"token": refreshToken}

	response, err := PerformHTTPPost(url, endpoint, token, requestBody)
	if err != nil {
		return false, err
	}
	defer response.Body.Close()

	var result GenericResponse
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return false, err
	}

	if result.Message == "Token revoked" {
		return true, nil
	}
	return false, fmt.Errorf("Token revocation failed: %s", result.Message)
}

func SetUserPassword(url, token, username, password string) (*GenericResponse, error) {
	endpoint := fmt.Sprintf("api/Authentication/%s/passwordreset", username)
	requestBody := map[string]string{"password": password}

	response, err := PerformHTTPPost(url, endpoint, token, requestBody)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result GenericResponse
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}

	if result.Message == "New password set" {
		return &result, nil
	}
	return nil, fmt.Errorf("Request failed: %s", result.Message)
}
