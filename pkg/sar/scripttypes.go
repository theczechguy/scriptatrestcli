package sar

import (
	"encoding/json"
	"fmt"
	"strconv"
)

type ScriptType struct {
	ID             int    `json:"id"`
	Name           string `json:"name"`
	Runner         string `json:"runner"`
	FileExtension  string `json:"fileExtension"`
	ScriptArgument string `json:"scriptArgument"`
}

func (c *APIClient) RegisterScriptType(name, runner, fileExtension, scriptArgument string) (*ScriptType, error) {

	endpoint := "api/script/type"
	requestBody := map[string]string{
		"name":           name,
		"runner":         runner,
		"fileExtension":  fileExtension,
		"scriptArgument": scriptArgument,
	}

	response, err := PerformHTTPPost(c.URL, endpoint, c.Token, requestBody)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result ScriptType
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (c *APIClient) DeleteScriptType(Id string) (bool, error) {

	endpoint := fmt.Sprintf("api/script/type/%s", Id)
	response, err := PerformHTTPDelete(c.URL, endpoint, c.Token)
	if err != nil {
		return false, err
	}
	defer response.Body.Close()
	return true, nil
}

func (c *APIClient) ListScriptTypes() ([]ScriptType, error) {
	endpoint := "api/script/type"
	response, err := PerformHTTPGet(c.URL, endpoint, c.Token)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result []ScriptType
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (c *APIClient) GetScriptType(Id int) (*ScriptType, error) {
	endpoint := fmt.Sprintf("api/script/type/%s", strconv.Itoa(Id))
	response, err := PerformHTTPGet(c.URL, endpoint, c.Token)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result ScriptType
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (c *APIClient) UpdateScriptType(id int, name, runner, fileExtension, scriptArgument string) (*ScriptType, error) {

	endpoint := fmt.Sprintf("api/script/type/%s", strconv.Itoa(id))
	requestBody := make(map[string]string)

	if name != "" {
		requestBody["name"] = name
	}
	if runner != "" {
		requestBody["runner"] = runner
	}
	if fileExtension != "" {
		requestBody["fileExtension"] = fileExtension
	}
	if scriptArgument != "" {
		requestBody["scriptArgument"] = scriptArgument
	}

	response, err := PerformHTTPPut(c.URL, endpoint, c.Token, requestBody)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result ScriptType
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}
