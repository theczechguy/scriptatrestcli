package sar

import (
	"encoding/json"
	"fmt"
)

type AgentCapability struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

func (c *APIClient) AssignCapabilityToAgent(agentId, capabilityId string) (*GenericResponse, error) {

	endpoint := fmt.Sprintf("api/agent/%s/capability/%s", agentId, capabilityId)
	response, err := PerformHTTPPost(c.URL, endpoint, c.Token, nil)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result GenericResponse
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}
func (c *APIClient) UnAssignCapabilityFromAgent(agentId, capabilityId string) (map[string]interface{}, error) {

	endpoint := fmt.Sprintf("api/agent/%s/capability/%s", agentId, capabilityId)
	response, err := PerformHTTPDelete(c.URL, endpoint, c.Token)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result map[string]interface{}
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (c *APIClient) RemoveCapabilityFromAgent(agentId, capabilityId string) (*GenericResponse, error) {

	endpoint := fmt.Sprintf("api/agent/%s/capability/%s", agentId, capabilityId)
	response, err := PerformHTTPDelete(c.URL, endpoint, c.Token)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result GenericResponse
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (c *APIClient) ListAgentCapabilities() (*[]AgentCapability, error) {

	endpoint := "api/agentcapability"
	response, err := PerformHTTPGet(c.URL, endpoint, c.Token)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result []AgentCapability
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (c *APIClient) RegisterAgentCapability(name, description string) (*AgentCapability, error) {

	endpoint := "api/AgentCapability"
	requestBody := map[string]string{
		"name":        name,
		"description": description,
	}
	response, err := PerformHTTPPost(c.URL, endpoint, c.Token, requestBody)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	var result AgentCapability
	err = json.NewDecoder(response.Body).Decode(&result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

// func (c *APIClient) DeleteCapability(capabilityId string) (*GenericResponse, error) {

// 	endpoint := fmt.Sprintf("api/agent/%s/capability/%s", agentId, capabilityId)
// 	response, err := PerformHTTPDelete(c.URL, endpoint, c.Token)
// 	if err != nil {
// 		return nil, err
// 	}
// 	defer response.Body.Close()

// 	var result GenericResponse
// 	err = json.NewDecoder(response.Body).Decode(&result)
// 	if err != nil {
// 		return nil, err
// 	}
// 	return &result, nil
// }
