package utils

import "regexp"

func ValidateJobID(jobID string) bool {
	// Regular expression pattern for a GUID.
	pattern := `^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$`
	match, _ := regexp.MatchString(pattern, jobID)
	return match
}
