package utils

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

type Config struct {
	URL string `json:"url"`
}

const configFileName = "config.json"

func SaveConfig(config Config) error {
	configBytes, err := json.Marshal(config)
	if err != nil {
		return fmt.Errorf("Error marshaling configuration: %v", err)
	}

	err = ioutil.WriteFile(configFileName, configBytes, 0644)
	if err != nil {
		return fmt.Errorf("Error writing configuration file: %v", err)
	}

	return nil
}

func LoadConfig() (Config, error) {
	wd, err := os.Getwd()
	if err != nil {
		return Config{}, err
	}

	configFilePath := filepath.Join(wd, configFileName)
	configData, err := ioutil.ReadFile(configFilePath)
	if err != nil {
		return Config{}, fmt.Errorf("Error reading configuration file: %v", err)
	}

	var config Config
	err = json.Unmarshal(configData, &config)
	if err != nil {
		return Config{}, fmt.Errorf("Error unmarshaling configuration: %v", err)
	}

	return config, nil
}

type AppSettings struct {
	AuthenticationTokens
	Config
}

func GetConnectionSettings() (AppSettings, error) {
	config, err := LoadConfig()
	if err != nil {
		return AppSettings{}, err
	}

	tokens, err := GetTokens()
	if err != nil {
		return AppSettings{}, fmt.Errorf("Error retrieving tokens: %v", err)
	}

	return AppSettings{
		AuthenticationTokens: tokens,
		Config:               config,
	}, nil
}
