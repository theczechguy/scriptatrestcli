package utils

import (
	"time"

	"github.com/zalando/go-keyring"
)

const (
	keyringService = "sarcli"
	tokenKey       = "token"
	tokenExpKey    = "tokenExp"
	refreshKey     = "refresh_token"
	refreshExpKey  = "refresh_tokenExp"
)

type AuthenticationTokens struct {
	Token                  string    `json:"token"`
	TokenExpiration        time.Time `json:"tokenExpiration"`
	RefreshToken           string    `json:"refreshToken"`
	RefreshTokenExpiration time.Time `json:"refreshTokenExpiration"`
}

func SaveTokens(tokens AuthenticationTokens) error {
	err := keyring.Set(keyringService, tokenKey, tokens.Token)
	if err != nil {
		return err
	}

	err = keyring.Set(keyringService, tokenExpKey, tokens.TokenExpiration.Format(time.RFC3339))
	if err != nil {
		return err
	}

	err = keyring.Set(keyringService, refreshKey, tokens.RefreshToken)
	if err != nil {
		return err
	}

	err = keyring.Set(keyringService, refreshExpKey, tokens.RefreshTokenExpiration.Format(time.RFC3339))
	if err != nil {
		return err
	}

	return nil
}

func GetTokens() (AuthenticationTokens, error) {
	token, err := keyring.Get(keyringService, tokenKey)
	if err != nil {
		return AuthenticationTokens{}, err
	}

	tokenExpStr, err := keyring.Get(keyringService, tokenExpKey)
	if err != nil {
		return AuthenticationTokens{}, err
	}

	refreshToken, err := keyring.Get(keyringService, refreshKey)
	if err != nil {
		return AuthenticationTokens{}, err
	}

	refreshExpStr, err := keyring.Get(keyringService, refreshExpKey)
	if err != nil {
		return AuthenticationTokens{}, err
	}

	tokenExp, err := time.Parse(time.RFC3339, tokenExpStr)
	if err != nil {
		return AuthenticationTokens{}, err
	}

	refreshExp, err := time.Parse(time.RFC3339, refreshExpStr)
	if err != nil {
		return AuthenticationTokens{}, err
	}

	return AuthenticationTokens{
		Token:                  token,
		TokenExpiration:        tokenExp,
		RefreshToken:           refreshToken,
		RefreshTokenExpiration: refreshExp,
	}, nil
}

func AreTokensExpired(tokens AuthenticationTokens) bool {
	// Check if the access token has expired
	if tokens.TokenExpiration.IsZero() || time.Now().After(tokens.TokenExpiration) {
		return true
	}

	// Check if the refresh token has expired
	if tokens.RefreshTokenExpiration.IsZero() || time.Now().After(tokens.RefreshTokenExpiration) {
		return true
	}

	// Tokens are not expired
	return false
}
