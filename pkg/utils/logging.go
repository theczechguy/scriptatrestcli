package utils

import (
	"encoding/json"
	"fmt"
)

func PrettyPrint(obj interface{}) {
	// Convert the object to pretty-printed JSON
	jsonData, err := json.MarshalIndent(obj, "", "    ")
	if err != nil {
		fmt.Println("Error marshaling JSON:", err)
		return
	}

	fmt.Println(string(jsonData))
}
