package utils

import (
	"fmt"

	"github.com/spf13/cobra"
)

func VerifyAndGetValueFromFlag(cmd *cobra.Command, flagName string) (string, error) {
	value, err := cmd.Flags().GetString(flagName)
	if err != nil {
		return "", fmt.Errorf("Error getting '%s' flag: %v", flagName, err)
	}
	if value == "" {
		return "", fmt.Errorf("You must provide a non-empty '%s'", flagName)
	}
	return value, nil
}
