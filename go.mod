module gitlab.com/theczechguy/scriptatrestcli

go 1.19

require (
	github.com/alessio/shellescape v1.4.2 // indirect
	github.com/danieljoos/wincred v1.2.1 // indirect
	github.com/godbus/dbus/v5 v5.1.0 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/cobra v1.8.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/zalando/go-keyring v0.2.3 // indirect
	golang.org/x/sys v0.16.0 // indirect
)
