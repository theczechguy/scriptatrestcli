/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"
)

// loginCmd represents the login command
var loginCmd = &cobra.Command{
	Use:   "login",
	Short: "Performs authentication against SAR server and retrieves authentication tokens",
	Run: func(cmd *cobra.Command, args []string) {
		url, _ := cmd.Flags().GetString("url")
		username, _ := cmd.Flags().GetString("username")
		password, _ := cmd.Flags().GetString("password")

		response, err := sar.Login(url, username, password)
		if err != nil {
			log.Fatalf("Authentication failed: %s", err)
		}

		err = saveConfig(url)
		if err != nil {
			fmt.Println(err)
		}
		tokens := utils.AuthenticationTokens{
			Token:        response.Token,
			RefreshToken: response.RefreshToken,
		}
		utils.SaveTokens(tokens)
		fmt.Printf("Login to %s successful\n", url)
	},
}

func init() {
	loginCmd.Flags().StringP("username", "u", "", "Username for login")
	loginCmd.Flags().StringP("password", "p", "", "Password for login")
	loginCmd.Flags().String("url", "", "SAR server URL")
	rootCmd.AddCommand(loginCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// loginCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// loginCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func saveConfig(url string) error {
	config := utils.Config{
		URL: url,
	}
	err := utils.SaveConfig(config)
	if err != nil {
		log.Fatal(err)
	}
	return nil
}
