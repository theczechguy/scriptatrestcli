/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package Script

import (
	"log"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"
)

// ScriptUpdateCmd represents the ScriptUpdate command
var ScriptUpdateCmd = &cobra.Command{
	Use:   "update",
	Short: "Update any property of script",
	Long: `Update any property of script. 
This command requires one argument: scriptid, which must be a non-negative integer.`,
	Example: `sarcli script update 1 --name "new name" --type 2 --timeout 60 --filepath /path/to/file`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			log.Fatalf("You must provide script id")
		}
		scriptidStr := args[0]
		scriptId, err := strconv.Atoi(scriptidStr)
		if err != nil || scriptId < 0 {
			log.Fatalf("Invalid script id: %s. It must be a non-negative integer.", scriptidStr)
		}
		scriptName, err := utils.VerifyAndGetValueFromFlag(cmd, "name")
		if err != nil {
			log.Println("Name will not be updated")
		} else {
			log.Println("Name will be updated")
		}
		scriptType, err := utils.VerifyAndGetValueFromFlag(cmd, "type")
		if err != nil {
			log.Println("Type will not be updated")
		} else {
			log.Println("Type will be updated")
		}
		scriptTimeout, err := utils.VerifyAndGetValueFromFlag(cmd, "timeout")
		if err != nil {
			log.Println("Timeout will not be updated")
		} else {
			log.Println("Timeout will be updated")
		}
		filePath, err := utils.VerifyAndGetValueFromFlag(cmd, "filepath")
		encodedContent := ""
		if err != nil {
			log.Println("Content will not be updated")
		} else {
			log.Println("Content will be updated")
			encodedContent, err = utils.EncodeFileToBase64(filePath)
			if err != nil {
				log.Fatalf("File loading failed: %s", err)
			}
		}

		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}
		response, err := apiClient.UpdateScript(scriptId, scriptName, scriptType, encodedContent, scriptTimeout)
		if err != nil {
			log.Fatalf("Script update failed: %s", err)
		}
		utils.PrettyPrint(response)
	},
}

func init() {
	ScriptUpdateCmd.Flags().StringP("name", "n", "", "New name of the script")
	ScriptUpdateCmd.Flags().StringP("type", "p", "", "New id of script type")
	ScriptUpdateCmd.Flags().StringP("timeout", "t", "", "New timeout for script execution in seconds")
	ScriptUpdateCmd.Flags().StringP("filepath", "f", "", "Path to file containing new script content")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// ScriptUpdateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// ScriptUpdateCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
