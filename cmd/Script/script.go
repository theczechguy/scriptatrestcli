/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package Script

import (
	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/cmd/Script/ScriptType"
)

// scriptCmd represents the script command
var ScriptCmd = &cobra.Command{
	Use:   "script",
	Short: "Script related operations",
	Run: func(cmd *cobra.Command, args []string) {
	},
}

func init() {
	ScriptCmd.AddCommand(ScriptRegisterCmd)
	ScriptCmd.AddCommand(ScriptDeleteCmd)
	ScriptCmd.AddCommand(ScriptListCmd)
	ScriptCmd.AddCommand(ScriptGetCmd)
	ScriptCmd.AddCommand(ScriptUpdateCmd)
	ScriptCmd.AddCommand(ScriptType.ScriptTypeCmd)
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// scriptCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// scriptCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
