/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package ScriptType

import (
	"log"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"
)

// ScriptTypeUpdateCmd represents the ScriptTypeUpdate command
var ScriptTypeUpdateCmd = &cobra.Command{
	Use:     "update",
	Short:   "Update any property of a script type",
	Example: `  sarcli script type update 1 --name "New name" --runner "New runner" --fileextension "New file extension" --argument "New argument"`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			log.Fatalf("You must provide script type id")
		}
		scripttypeidStr := args[0]
		typeId, err := strconv.Atoi(scripttypeidStr)
		if err != nil || typeId < 0 {
			log.Fatalf("Invalid script id: %s. It must be a non-negative integer.", scripttypeidStr)
		}
		typeName, err := utils.VerifyAndGetValueFromFlag(cmd, "name")
		if err != nil {
			log.Println("Name will not be updated")
		} else {
			log.Println("Name will be updated")
		}
		typeRunner, err := utils.VerifyAndGetValueFromFlag(cmd, "runner")
		if err != nil {
			log.Println("Runner will not be updated")
		} else {
			log.Println("Runner will be updated")
		}
		typeFileExtension, err := utils.VerifyAndGetValueFromFlag(cmd, "fileextension")
		if err != nil {
			log.Println("File extension will not be updated")
		} else {
			log.Println("File extension will be updated")
		}
		typeArgument, err := utils.VerifyAndGetValueFromFlag(cmd, "argument")
		if err != nil {
			log.Println("Argument will not be updated")
		} else {
			log.Println("Argument will be updated")
		}

		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}
		response, err := apiClient.UpdateScriptType(typeId, typeName, typeRunner, typeFileExtension, typeArgument)
		if err != nil {
			log.Fatalf("Request failed: %s", err)
		}
		utils.PrettyPrint(response)
	},
}

func init() {

	ScriptTypeUpdateCmd.Flags().StringP("name", "n", "", "Name of the script type")
	ScriptTypeUpdateCmd.Flags().StringP("runner", "r", "", "Runner of the script type")
	ScriptTypeUpdateCmd.Flags().StringP("fileextension", "f", "", "File extension of the script type")
	ScriptTypeUpdateCmd.Flags().StringP("argument", "a", "", "Argument of the script type")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// ScriptTypeUpdateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// ScriptTypeUpdateCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
