/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package ScriptType

import (
	"log"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"
)

// ScriptTypeGetCmd represents the ScriptTypeGet command
var ScriptTypeGetCmd = &cobra.Command{
	Use:   "get",
	Short: "Get script type by ID",
	Run: func(cmd *cobra.Command, args []string) {
		id, err := utils.VerifyAndGetValueFromFlag(cmd, "id")
		if err != nil {
			log.Fatalf(err.Error())
		}
		typeId, err := strconv.Atoi(id)
		if err != nil || typeId < 0 {
			log.Fatalf("Invalid script type id: %s. It must be a non-negative integer.", id)
		}
		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}

		response, err := apiClient.GetScriptType(typeId)
		if err != nil {
			log.Fatalf("Script type retrieval failed: %s", err)
		}
		utils.PrettyPrint(response)
	},
}

func init() {

	ScriptTypeGetCmd.Flags().StringP("id", "i", "", "ID of the script type")
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// ScriptTypeGetCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// ScriptTypeGetCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
