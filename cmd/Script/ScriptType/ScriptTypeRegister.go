/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package ScriptType

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"
)

// ScriptTypeRegisterCmd represents the ScriptTypeRegister command
var ScriptTypeRegisterCmd = &cobra.Command{
	Use:   "register",
	Short: "Register a new script type",
	Run: func(cmd *cobra.Command, args []string) {
		name, err := utils.VerifyAndGetValueFromFlag(cmd, "name")
		if err != nil {
			log.Fatalf(err.Error())
		}
		runner, err := utils.VerifyAndGetValueFromFlag(cmd, "runner")
		if err != nil {
			log.Fatalf(err.Error())
		}
		fileExtension, err := utils.VerifyAndGetValueFromFlag(cmd, "fileextension")
		if err != nil {
			log.Fatalf(err.Error())
		}
		scriptArgument, err := utils.VerifyAndGetValueFromFlag(cmd, "scriptargument")
		if err != nil {
			log.Fatalf(err.Error())
		}

		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}
		response, err := apiClient.RegisterScriptType(name, runner, fileExtension, scriptArgument)
		if err != nil {
			log.Fatalf("Request failed: %s", err)
		}
		utils.PrettyPrint(response)
	},
}

func init() {
	ScriptTypeRegisterCmd.Flags().StringP("name", "n", "", "Name of the script type")
	ScriptTypeRegisterCmd.Flags().StringP("runner", "r", "", "Runner of the script type")
	ScriptTypeRegisterCmd.Flags().StringP("fileextension", "f", "", "File extension of the script type")
	ScriptTypeRegisterCmd.Flags().StringP("scriptargument", "s", "", "Script argument of the script type")
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// ScriptTypeRegisterCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// ScriptTypeRegisterCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
