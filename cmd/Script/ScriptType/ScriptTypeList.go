/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package ScriptType

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"
)

// ScriptTypeListCmd represents the ScriptTypeList command
var ScriptTypeListCmd = &cobra.Command{
	Use:   "list",
	Short: "List all script types",
	Run: func(cmd *cobra.Command, args []string) {
		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}

		response, err := apiClient.ListScriptTypes()
		if err != nil {
			log.Fatalf("Script type list failed: %s", err)
		}
		utils.PrettyPrint(response)
	},
}

func init() {
	ScriptTypeCmd.AddCommand(ScriptTypeListCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// ScriptTypeListCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// ScriptTypeListCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
