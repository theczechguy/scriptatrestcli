/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package ScriptType

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"
)

// ScriptTypeDeleteCmd represents the ScriptTypeDelete command
var ScriptTypeDeleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "Delete specified script type",
	Run: func(cmd *cobra.Command, args []string) {
		id, err := utils.VerifyAndGetValueFromFlag(cmd, "id")
		if err != nil {
			log.Fatalf(err.Error())
		}
		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}

		_, err = apiClient.DeleteScriptType(id)
		if err != nil {
			log.Fatalf("Script type deletion failed: %s", err)
		}
		log.Println("Script type deleted")
	},
}

func init() {

	ScriptTypeDeleteCmd.Flags().StringP("id", "i", "", "ID of the script type")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// ScriptTypeDeleteCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// ScriptTypeDeleteCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
