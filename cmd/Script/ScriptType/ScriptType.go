/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package ScriptType

import (
	"github.com/spf13/cobra"
)

// ScriptTypeCmd represents the ScriptType command
var ScriptTypeCmd = &cobra.Command{
	Use:   "type",
	Short: "ScriptType operations",
	Run: func(cmd *cobra.Command, args []string) {
	},
}

func init() {
	ScriptTypeCmd.AddCommand(ScriptTypeRegisterCmd)
	ScriptTypeCmd.AddCommand(ScriptTypeDeleteCmd)
	ScriptTypeCmd.AddCommand(ScriptTypeGetCmd)
	ScriptTypeCmd.AddCommand(ScriptTypeUpdateCmd)
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// ScriptTypeCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// ScriptTypeCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
