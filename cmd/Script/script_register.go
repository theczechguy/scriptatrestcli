/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package Script

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"
)

// registerCmd represents the register command
var ScriptRegisterCmd = &cobra.Command{
	Use:     "register",
	Short:   "Register a new script",
	Example: `scriptatrestcli script register -f /path/to/script.sh -p 1 -t 60 -n "My script"`,
	Run: func(cmd *cobra.Command, args []string) {
		filePath, err := utils.VerifyAndGetValueFromFlag(cmd, "filepath")
		if err != nil {
			log.Fatalf(err.Error())
		}
		scriptType, err := utils.VerifyAndGetValueFromFlag(cmd, "type")
		if err != nil {
			log.Fatalf(err.Error())
		}
		timeout, err := utils.VerifyAndGetValueFromFlag(cmd, "timeout")
		if err != nil {
			log.Fatalf(err.Error())
		}
		name, err := utils.VerifyAndGetValueFromFlag(cmd, "name")
		if err != nil {
			log.Fatalf(err.Error())
		}
		encodedContent, err := utils.EncodeFileToBase64(filePath)
		if err != nil {
			log.Fatalf("File loading failed: %s", err)
		}
		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}
		response, err := apiClient.RegisterScript(name, scriptType, encodedContent, timeout)
		if err != nil {
			log.Fatalf("Request failed: %s", err)
		}
		utils.PrettyPrint(response)
	},
}

func init() {
	ScriptRegisterCmd.Flags().StringP("filepath", "f", "", "Path to file containing script")
	ScriptRegisterCmd.Flags().StringP("type", "p", "", "Id of script type")
	ScriptRegisterCmd.Flags().StringP("timeout", "t", "", "Timeout for script execution in seconds")
	ScriptRegisterCmd.Flags().StringP("name", "n", "", "Name of the script")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// registerCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// registerCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
