/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package Script

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"
)

// ScriptDeleteCmd represents the ScriptDelete command
var ScriptDeleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "Delete specified script",
	Run: func(cmd *cobra.Command, args []string) {
		scriptId, err := utils.VerifyAndGetValueFromFlag(cmd, "id")
		if err != nil {
			log.Fatalf(err.Error())
		}
		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}
		result, err := apiClient.DeleteScript(scriptId)
		if err != nil {
			log.Fatalf("Request failed: %s", err)
		}
		fmt.Println("Script deleted: ", result)
	},
}

func init() {
	ScriptDeleteCmd.Flags().StringP("id", "i", "", "Script ID")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// ScriptDeleteCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// ScriptDeleteCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
