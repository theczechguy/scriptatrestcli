/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package User

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/cmd/user/UserRoleAssignment"
)

// userCmd represents the user command
var UserCmd = &cobra.Command{
	Use:   "user",
	Short: "User related operations",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("user called")
	},
}

func init() {
	UserCmd.AddCommand(UserRemoveCmd)
	UserCmd.AddCommand(UserListCmd)
	UserCmd.AddCommand(UserFindCmd)
	UserCmd.AddCommand(CreateCmd)
	UserCmd.AddCommand(ApproveCmd)
	UserCmd.AddCommand(UserRoleAssignment.RoleAssignmentCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// userCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// userCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
