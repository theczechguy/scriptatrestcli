/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package User

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"
)

// approveCmd represents the approve command
var ApproveCmd = &cobra.Command{
	Use:   "approve",
	Short: "Approve newly created user",
	Run: func(cmd *cobra.Command, args []string) {
		username, err := utils.VerifyAndGetValueFromFlag(cmd, "username")
		if err != nil {
			log.Fatalf(err.Error())
		}

		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}
		response, err := apiClient.ApproveUser(username)
		if err != nil {
			log.Fatalf("User approval failed: %s", err)
		}
		utils.PrettyPrint(response)
	},
}

func init() {
	ApproveCmd.Flags().StringP("username", "u", "", "username")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// approveCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// approveCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
