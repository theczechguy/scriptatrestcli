/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package User

import (
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"

	"log"

	"github.com/spf13/cobra"
)

// createCmd represents the create command
var CreateCmd = &cobra.Command{
	Use:   "create",
	Short: "create new user",
	Run: func(cmd *cobra.Command, args []string) {
		username, err := utils.VerifyAndGetValueFromFlag(cmd, "username")
		if err != nil {
			log.Fatalf(err.Error())
		}
		password, err := utils.VerifyAndGetValueFromFlag(cmd, "password")
		if err != nil {
			log.Fatalf(err.Error())
		}
		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}

		registerResponse, err := apiClient.RegisterUser(username, password)
		if err != nil {
			log.Fatalf("User registration failed: %s", err)
		}
		utils.PrettyPrint(registerResponse)
	},
}

func init() {
	CreateCmd.Flags().StringP("username", "u", "", "username")
	CreateCmd.Flags().StringP("password", "p", "", "password")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// createCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// createCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
