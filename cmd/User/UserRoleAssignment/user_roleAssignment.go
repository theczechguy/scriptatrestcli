/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package UserRoleAssignment

import (
	"fmt"

	"github.com/spf13/cobra"
)

// roleAssignmentCmd represents the roleAssignment command
var RoleAssignmentCmd = &cobra.Command{
	Use:   "roleAssignment",
	Short: "User role assignment operations",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("roleAssignment called")
	},
}

func init() {
	RoleAssignmentCmd.AddCommand(AddCmd)
	RoleAssignmentCmd.AddCommand(RemoveCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// roleAssignmentCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// roleAssignmentCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
