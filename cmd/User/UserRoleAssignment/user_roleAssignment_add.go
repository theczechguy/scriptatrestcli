/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package UserRoleAssignment

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"
)

// addCmd represents the add command
var AddCmd = &cobra.Command{
	Use:   "add",
	Short: "Add a new role assignment to a user",
	Run: func(cmd *cobra.Command, args []string) {
		username, err := cmd.Flags().GetString("username")
		if err != nil {
			log.Fatalf("You must provide username")
		}
		rolename, err := cmd.Flags().GetString("rolename")
		if err != nil {
			log.Fatalf("You must provide rolename")
		}
		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}
		response, err := apiClient.AssignRoleToUser(username, rolename)
		if err != nil {
			log.Fatalf("Role assignemnt failed: %s", err)
		}
		utils.PrettyPrint(response)
	},
}

func init() {
	AddCmd.Flags().StringP("username", "u", "", "username")
	AddCmd.Flags().StringP("rolename", "r", "", "rolename")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// addCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// addCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
