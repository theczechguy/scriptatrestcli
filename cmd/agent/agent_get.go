/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package agent

import (
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"

	"log"

	"github.com/spf13/cobra"
)

// getCmd represents the get command
var agentGetCmd = &cobra.Command{
	Use:   "get",
	Short: "Get agent by id",
	Run: func(cmd *cobra.Command, args []string) {
		agentid, err := utils.VerifyAndGetValueFromFlag(cmd, "agentid")
		if err != nil {
			log.Fatalf(err.Error())
		}
		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}

		response, err := apiClient.GetAgentById(agentid)
		if err != nil {
			log.Fatalf("Agent list failed: %s", err)
		}
		utils.PrettyPrint(response)
	},
}

func init() {
	agentGetCmd.Flags().StringP("agentid", "", "", "agentid")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// getCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// getCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
