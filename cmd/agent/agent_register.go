/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package agent

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"
)

// registerCmd represents the register command
var agentRegisterCmd = &cobra.Command{
	Use:   "register",
	Short: "Register new agent",
	Run: func(cmd *cobra.Command, args []string) {
		agentName, err := utils.VerifyAndGetValueFromFlag(cmd, "name")
		if err != nil {
			log.Fatalf(err.Error())
		}
		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}
		response, err := apiClient.RegisterAgent(agentName)
		if err != nil {
			log.Fatalf("Agent registration failed: %s", err)
		}
		utils.PrettyPrint(response)
	},
}

func init() {
	agentRegisterCmd.Flags().StringP("name", "n", "", "Agent Name")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// registerCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// registerCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
