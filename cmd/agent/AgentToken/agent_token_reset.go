/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package AgentToken

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"
)

// resetCmd represents the reset command
var AgentTokenResetCmd = &cobra.Command{
	Use:   "reset",
	Short: "Generate new connection token without using refresh token",
	Run: func(cmd *cobra.Command, args []string) {
		agentId, err := utils.VerifyAndGetValueFromFlag(cmd, "agentid")
		if err != nil {
			log.Fatalf(err.Error())
		}
		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}
		response, err := apiClient.ResetAgentToken(agentId)
		if err != nil {
			log.Fatalf("Token reset failed: %s", err)
		}
		utils.PrettyPrint(response)
	},
}

func init() {
	AgentTokenResetCmd.Flags().StringP("agentid", "u", "", "Agent ID")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// resetCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// resetCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
