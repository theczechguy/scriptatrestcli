/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package AgentToken

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"
)

// refreshCmd represents the refresh command
var AgentTokenRefreshCmd = &cobra.Command{
	Use:   "refresh",
	Short: "Generate a new connection token using refresh token",
	Run: func(cmd *cobra.Command, args []string) {
		refreshToken, err := utils.VerifyAndGetValueFromFlag(cmd, "refreshToken")
		if err != nil {
			log.Fatalf(err.Error())
		}
		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}
		response, err := apiClient.RefreshAgentToken(refreshToken)
		if err != nil {
			log.Fatalf("Token refresh failed: %s", err)
		}
		utils.PrettyPrint(response)
	},
}

func init() {
	AgentTokenRefreshCmd.Flags().StringP("refreshToken", "r", "", "Refresh token")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// refreshCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// refreshCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
