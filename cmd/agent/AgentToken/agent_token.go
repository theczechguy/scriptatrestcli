/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package AgentToken

import (
	"github.com/spf13/cobra"
)

// tokenCmd represents the token command
var AgentTokenCmd = &cobra.Command{
	Use:   "token",
	Short: "Agent token operations",
	Run: func(cmd *cobra.Command, args []string) {
	},
}

func init() {
	AgentTokenCmd.AddCommand(AgentTokenRefreshCmd)
	AgentTokenCmd.AddCommand(AgentTokenResetCmd)
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// tokenCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// tokenCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
