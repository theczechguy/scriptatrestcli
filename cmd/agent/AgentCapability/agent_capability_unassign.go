/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package AgentCapability

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"
)

// AgentCapabilityUnassignCmd represents the AgentCapabilityUnassign command
var AgentCapabilityUnassignCmd = &cobra.Command{
	Use:   "unassign",
	Short: "Remove capability assignment from agent",
	Run: func(cmd *cobra.Command, args []string) {
		agentid, err := utils.VerifyAndGetValueFromFlag(cmd, "agentid")
		if err != nil {
			log.Fatalf(err.Error())
		}
		capabilityid, err := utils.VerifyAndGetValueFromFlag(cmd, "capabilityid")
		if err != nil {
			log.Fatalf(err.Error())
		}
		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}
		response, err := apiClient.UnAssignCapabilityFromAgent(agentid, capabilityid)
		if err != nil {
			log.Fatalf("Request failed: %s", err)
		}
		utils.PrettyPrint(response)
	},
}

func init() {
	AgentCapabilityUnassignCmd.Flags().StringP("agentid", "a", "", "Agent ID")
	AgentCapabilityUnassignCmd.Flags().StringP("capabilityid", "c", "", "Capability ID")
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// AgentCapabilityUnassignCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// AgentCapabilityUnassignCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
