/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package AgentCapability

import (
	"github.com/spf13/cobra"
)

// capabilityCmd represents the capability command
var AgentCapabilityCmd = &cobra.Command{
	Use:   "capability",
	Short: "Agent capability operations",
	Run: func(cmd *cobra.Command, args []string) {
	},
}

func init() {
	// AgentCapabilityCmd.AddCommand(AgentCapabilityDeleteCmd)
	AgentCapabilityCmd.AddCommand(AgentCapabilityAssignCmd)
	AgentCapabilityCmd.AddCommand(AgentCapabilityListCmd)
	AgentCapabilityCmd.AddCommand(AgentCapabilityCreateCmd)
	AgentCapabilityCmd.AddCommand(AgentCapabilityUnassignCmd)
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// capabilityCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// capabilityCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
