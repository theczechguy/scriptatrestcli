/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package AgentCapability

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"
)

// AgentCapabilityListCmd represents the AgentCapabilityList command
var AgentCapabilityListCmd = &cobra.Command{
	Use:   "list",
	Short: "List all capabilities",
	Run: func(cmd *cobra.Command, args []string) {
		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}
		response, err := apiClient.ListAgentCapabilities()
		if err != nil {
			log.Fatalf("Request failed: %s", err)
		}
		// display message "No capabilities found" if response is empty
		if len(*response) == 0 {
			log.Println("No capabilities found")
		} else {
			utils.PrettyPrint(response)
		}
	},
}

func init() {

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// AgentCapabilityListCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// AgentCapabilityListCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
