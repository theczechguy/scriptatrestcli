/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package AgentCapability

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"
)

// AgentCapabilityCreateCmd represents the AgentCapabilityCreate command
var AgentCapabilityCreateCmd = &cobra.Command{
	Use:   "create",
	Short: "Create a new capability",
	Run: func(cmd *cobra.Command, args []string) {
		capName, err := utils.VerifyAndGetValueFromFlag(cmd, "name")
		if err != nil {
			log.Fatalf(err.Error())
		}
		capDescription, err := utils.VerifyAndGetValueFromFlag(cmd, "description")
		if err != nil {
			log.Fatalf(err.Error())
		}
		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}
		response, err := apiClient.RegisterAgentCapability(capName, capDescription)
		if err != nil {
			log.Fatalf("Request failed: %s", err)
		}
		utils.PrettyPrint(response)
	},
}

func init() {
	AgentCapabilityCreateCmd.Flags().StringP("name", "n", "", "Capability Name")
	AgentCapabilityCreateCmd.Flags().StringP("description", "d", "", "Capability Description")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// AgentCapabilityCreateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// AgentCapabilityCreateCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
