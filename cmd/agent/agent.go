/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package agent

import (
	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/cmd/agent/AgentCapability"
	"gitlab.com/theczechguy/scriptatrestcli/cmd/agent/AgentToken"
)

// agentCmd represents the agent command
var AgentCmd = &cobra.Command{
	Use:   "agent",
	Short: "Agent operations",
	Run: func(cmd *cobra.Command, args []string) {
	},
}

func init() {
	AgentCmd.AddCommand(AgentCapability.AgentCapabilityCmd)
	AgentCmd.AddCommand(AgentToken.AgentTokenCmd)
	AgentCmd.AddCommand(agentRegisterCmd)
	AgentCmd.AddCommand(agentListCmd)
	AgentCmd.AddCommand(agentGetCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// agentCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// agentCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
