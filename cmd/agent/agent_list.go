/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package agent

import (
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"

	"log"

	"github.com/spf13/cobra"
)

// listCmd represents the list command
var agentListCmd = &cobra.Command{
	Use:   "list",
	Short: "List all registered agents",
	Run: func(cmd *cobra.Command, args []string) {
		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}

		response, err := apiClient.ListAgents()
		if err != nil {
			log.Fatalf("Agent list failed: %s", err)
		}
		utils.PrettyPrint(response)
	},
}

func init() {

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// listCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// listCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
