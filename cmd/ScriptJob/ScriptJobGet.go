/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package ScriptJob

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"
)

// ScriptJobGetCmd represents the ScriptJobGet command
var ScriptJobGetCmd = &cobra.Command{
	Use:     "get",
	Short:   "Get script job by id",
	Example: `  sarcli scriptjob get "24b42b0c-80e9-4c8a-9b3b-e43e15fb36cf"`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			log.Fatalf("You must provide script job id")
		}
		jobIdStr := args[0]
		if !utils.ValidateJobID(jobIdStr) {
			log.Fatalf("Invalid job id")
		}
		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}

		response, err := apiClient.GetJob(jobIdStr)
		if err != nil {
			log.Fatalf("Request failed: %s", err)
		}
		utils.PrettyPrint(response)
	},
}

func init() {

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// ScriptJobGetCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// ScriptJobGetCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
