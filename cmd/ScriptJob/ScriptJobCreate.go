/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package ScriptJob

import (
	"encoding/json"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"
)

// ScriptJobCreateCmd represents the ScriptJobCreate command
var ScriptJobCreateCmd = &cobra.Command{
	Use:   "create",
	Short: "Schedule a new script job",
	Run: func(cmd *cobra.Command, args []string) {
		// I need to call a CreateJob function from sar.APIClient and pass the paramters to it
		// parameters are takken from a file which is passed as a flag
		// content of the file is a json object which needs to be parsed as a struct ScriptJobRequest and passed to the CreateJob function
		// Read the file path from the flag
		filePath, err := cmd.Flags().GetString("file")
		if err != nil {
			log.Fatal(err)
		}

		// Read the content of the file
		content, err := os.ReadFile(filePath)
		if err != nil {
			log.Fatal(err)
		}

		// Parse the content as a ScriptJobRequest struct
		var jobRequest sar.ScriptJobRequest
		err = json.Unmarshal(content, &jobRequest)
		if err != nil {
			log.Fatal(err)
		}
		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}

		response, err := apiClient.CreateJob(jobRequest)
		if err != nil {
			log.Fatalf("Request failed: %s", err)
		}
		utils.PrettyPrint(response)
	},
}

func init() {
	ScriptJobCreateCmd.Flags().StringP("file", "f", "", "Path to the file with the job request")
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// ScriptJobCreateCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// ScriptJobCreateCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
