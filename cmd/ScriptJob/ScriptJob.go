/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package ScriptJob

import (
	"github.com/spf13/cobra"
)

// ScriptJobCmd represents the ScriptJob command
var ScriptJobCmd = &cobra.Command{
	Use:   "job",
	Short: "Script job operations",
	Run: func(cmd *cobra.Command, args []string) {
	},
}

func init() {
	ScriptJobCmd.AddCommand(ScriptJobCreateCmd)
	ScriptJobCmd.AddCommand(ScriptJobGetCmd)
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// ScriptJobCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// ScriptJobCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
