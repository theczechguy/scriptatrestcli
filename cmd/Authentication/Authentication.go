/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package Authentication

import (
	"github.com/spf13/cobra"
)

// AuthenticationCmd represents the Authentication command
var AuthenticationCmd = &cobra.Command{
	Use:   "authentication",
	Short: "Authentication operations",
	Run: func(cmd *cobra.Command, args []string) {
	},
}

func init() {
	AuthenticationCmd.AddCommand(AuthenticationGenerateTokensCmd)
}
