/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package Authentication

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/sar"
	"gitlab.com/theczechguy/scriptatrestcli/pkg/utils"
)

// AuthenticationGenerateTokensCmd represents the AuthenticationGenerateTokens command
var AuthenticationGenerateTokensCmd = &cobra.Command{
	Use:   "generate-tokens",
	Short: "Generate a new set of tokens for a user",
	Run: func(cmd *cobra.Command, args []string) {
		username, err := utils.VerifyAndGetValueFromFlag(cmd, "username")
		if err != nil {
			log.Fatalf(err.Error())
		}
		password, err := utils.VerifyAndGetValueFromFlag(cmd, "password")
		if err != nil {
			log.Fatalf(err.Error())
		}

		conSettings, err := utils.GetConnectionSettings()
		if err != nil {
			log.Fatal(err)
		}
		apiClient := &sar.APIClient{
			URL:   conSettings.URL,
			Token: conSettings.Token,
		}
		response, err := apiClient.GenerateTokens(username, password)
		if err != nil {
			log.Fatalf("Request failed: %s", err)
		}
		utils.PrettyPrint(response)
	},
}

func init() {
	AuthenticationGenerateTokensCmd.Flags().StringP("username", "u", "", "Username")
	AuthenticationGenerateTokensCmd.Flags().StringP("password", "p", "", "Password")

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// AuthenticationGenerateTokensCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// AuthenticationGenerateTokensCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
