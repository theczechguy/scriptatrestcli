/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/theczechguy/scriptatrestcli/cmd/Authentication"
	"gitlab.com/theczechguy/scriptatrestcli/cmd/Script"
	"gitlab.com/theczechguy/scriptatrestcli/cmd/ScriptJob"
	"gitlab.com/theczechguy/scriptatrestcli/cmd/agent"
	User "gitlab.com/theczechguy/scriptatrestcli/cmd/user"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "sarcli",
	Short: "CLI interface for ScriptAtRest tool",
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	// rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.sarcli.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.AddCommand(agent.AgentCmd)
	rootCmd.AddCommand(User.UserCmd)
	rootCmd.AddCommand(healthcheckCmd)
	rootCmd.AddCommand(Script.ScriptCmd)
	rootCmd.AddCommand(ScriptJob.ScriptJobCmd)
	rootCmd.AddCommand(Authentication.AuthenticationCmd)
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
