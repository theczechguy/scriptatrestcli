/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package main

import "gitlab.com/theczechguy/scriptatrestcli/cmd"

func main() {
	cmd.Execute()
}
